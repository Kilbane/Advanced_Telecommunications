# Project 1: Web Proxy Server

The aim of this project was to write a web proxy server that implemented the
following features:
- Displaying and responding to HTTP and HTTPS requests
- Handling WebSocket connections
- Dynamically blocking selected URLs
- Caching HTTP requests
- Handling multiple requests simultateously with threading

## Dependencies
- Python

## To Run
``python server.py``

## Usage
When you start the program, you will be asked to provide a list of URLs to block.
Press q to start the server.

Once the server is running, go to your browser's network settings and configure
it to use a proxy listening on port 5050 at your local IP address.

Now, any HTTP or HTTPS requests you make in the browser will be sent to the
proxy server.

When the proxy receives a request, the request will be printed to the screen.
It will then be forwarded on to its destination, unless the destination is blocked.
